package fr.ajc.formationjava;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import fr.ajc.formation.java.CalculateurFactorielle;

class CalculateurFactorielTest {

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@Test
	void Factorielle_de_0_should_be_1() {
	
		assertEquals(1, CalculateurFactorielle.factorielle(0));
	}
	@Test
	void Factorielle_de_1_should_be_1() {
		
		assertEquals(1, CalculateurFactorielle.factorielle(1));
	}
	@Test
	void Factorielle_de_3_should_be_6() {
		
		assertEquals(6, CalculateurFactorielle.factorielle(3));
	}
	@Test
	void Factorielle_de_6_should_be_720() {
		
		assertEquals(720, CalculateurFactorielle.factorielle(6));
	}
	@Test
void Factorielle_de_0_is_1() {
	
	assertTrue( CalculateurFactorielle.factorielle(0)==1);
}
	@Test
	
void Factorielle_de_1_is_1() {
	
	assertTrue(CalculateurFactorielle.factorielle(1)==1);
}
	@Test
void Factorielle_de_3_is_6() {
	
	assertTrue( CalculateurFactorielle.factorielle(3)==6);
}
	@Test
void Factorielle_de_6_is_720() {
		
		assertTrue(CalculateurFactorielle.factorielle(6)==720 );
	}
	
	
	
}
